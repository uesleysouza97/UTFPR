class Fumante

	attr_accessor :nome, :anos, :n_dia, :preco, :qtd_cart, :total

	def initialize(nome, anos, n_dia, preco, qtd_cart)
		@nome, @anos, @n_dia, @preco, @qtd_cart = nome, anos, n_dia, preco, qtd_cart
	end

	def calc
		@total = (((@anos.to_i * 365) * @n_dia.to_i)/qtd_cart.to_i) * @preco.to_f
	end

	def to_s
		"#{@nome} já gastou #{@total} em cigarros em toda sua vida."
	end

end

puts "Nome:"
nome = gets.chomp
puts "Há quantos anos fuma?"
anos = gets.chomp
puts "Quantos cigarros fuma ao dia?"
qtd_d = gets.chomp
puts "Em média, quantos cigarros tem em uma carteira?"
qtd_c = gets.chomp
puts "Qual é o preço de uma carteira?"
preco = gets.chomp

fumante = Fumante.new(nome, anos, qtd_d, preco, qtd_c)
fumante.calc
puts fumante.to_s